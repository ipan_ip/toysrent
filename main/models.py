# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Admin(models.Model):
    no_ktp = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='no_ktp', primary_key=True)

    class Meta:
        managed = False
        db_table = 'admin'


class Alamat(models.Model):
    no_ktp_anggota = models.ForeignKey('Anggota', models.DO_NOTHING, db_column='no_ktp_anggota', primary_key=True)
    nama = models.CharField(max_length=255)
    jalan = models.CharField(max_length=255)
    nomor = models.IntegerField()
    kota = models.CharField(max_length=255)
    kodepos = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'alamat'
        unique_together = (('no_ktp_anggota', 'nama'),)


class Anggota(models.Model):
    no_ktp = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='no_ktp', primary_key=True)
    poin = models.FloatField()
    level = models.ForeignKey('LevelKeanggotaan', models.DO_NOTHING, db_column='level', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'anggota'


class Barang(models.Model):
    id_barang = models.CharField(primary_key=True, max_length=10)
    nama_item = models.ForeignKey('Item', models.DO_NOTHING, db_column='nama_item')
    warna = models.CharField(max_length=50, blank=True, null=True)
    url_foto = models.TextField(blank=True, null=True)
    kondisi = models.TextField()
    lama_penggunaan = models.IntegerField(blank=True, null=True)
    no_ktp_penyewa = models.ForeignKey(Anggota, models.DO_NOTHING, db_column='no_ktp_penyewa')

    class Meta:
        managed = False
        db_table = 'barang'


class BarangDikembalikan(models.Model):
    no_resi = models.ForeignKey('Pengiriman', models.DO_NOTHING, db_column='no_resi', primary_key=True)
    no_urut = models.CharField(max_length=10)
    id_barang = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'barang_dikembalikan'
        unique_together = (('no_resi', 'no_urut'),)


class BarangDikirim(models.Model):
    no_resi = models.ForeignKey('Pengiriman', models.DO_NOTHING, db_column='no_resi', primary_key=True)
    no_urut = models.CharField(max_length=10)
    id_barang = models.ForeignKey(Barang, models.DO_NOTHING, db_column='id_barang', blank=True, null=True)
    tanggal_review = models.DateField()
    review = models.TextField()

    class Meta:
        managed = False
        db_table = 'barang_dikirim'
        unique_together = (('no_resi', 'no_urut'),)


class BarangPesanan(models.Model):
    id_pemesanan = models.ForeignKey('Pemesanan', models.DO_NOTHING, db_column='id_pemesanan', primary_key=True)
    no_urut = models.CharField(max_length=10)
    tanggal_sewa = models.DateField()
    lama_sewa = models.IntegerField()
    tanggal_kembali = models.DateField(blank=True, null=True)
    status = models.ForeignKey('Status', models.DO_NOTHING, db_column='status')
    id_barang = models.ForeignKey(Barang, models.DO_NOTHING, db_column='id_barang', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'barang_pesanan'
        unique_together = (('id_pemesanan', 'no_urut'),)


class Chat(models.Model):
    id = models.CharField(primary_key=True, max_length=15)
    pesan = models.TextField()
    date_time = models.DateTimeField()
    no_ktp_anggota = models.ForeignKey(Anggota, models.DO_NOTHING, db_column='no_ktp_anggota')
    no_ktp_admin = models.ForeignKey(Admin, models.DO_NOTHING, db_column='no_ktp_admin')

    class Meta:
        managed = False
        db_table = 'chat'


class InfoBarangLevel(models.Model):
    id_barang = models.ForeignKey(Barang, models.DO_NOTHING, db_column='id_barang', primary_key=True)
    nama_level = models.ForeignKey('LevelKeanggotaan', models.DO_NOTHING, db_column='nama_level')
    harga_sewa = models.FloatField()
    porsi_royalti = models.FloatField()

    class Meta:
        managed = False
        db_table = 'info_barang_level'
        unique_together = (('id_barang', 'nama_level'),)


class Item(models.Model):
    nama = models.CharField(primary_key=True, max_length=255)
    deskripsi = models.TextField(blank=True, null=True)
    usia_dari = models.IntegerField()
    usia_sampai = models.IntegerField()
    bahan = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item'


class Kategori(models.Model):
    nama = models.CharField(primary_key=True, max_length=255)
    level = models.IntegerField()
    sub_dari = models.ForeignKey('self', models.DO_NOTHING, db_column='sub_dari', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kategori'


class KategoriItem(models.Model):
    nama_item = models.ForeignKey(Item, models.DO_NOTHING, db_column='nama_item', primary_key=True)
    nama_kategori = models.ForeignKey(Kategori, models.DO_NOTHING, db_column='nama_kategori')

    class Meta:
        managed = False
        db_table = 'kategori_item'
        unique_together = (('nama_item', 'nama_kategori'),)


class LevelKeanggotaan(models.Model):
    nama_level = models.CharField(primary_key=True, max_length=20)
    minimum_poin = models.FloatField()
    deskripsi = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'level_keanggotaan'


class Pemesanan(models.Model):
    id_pemesanan = models.CharField(primary_key=True, max_length=10)
    datetime_pesanan = models.DateTimeField()
    kuantitas_barang = models.IntegerField()
    harga_sewa = models.FloatField(blank=True, null=True)
    ongkos = models.FloatField(blank=True, null=True)
    no_ktp_pemesan = models.ForeignKey(Anggota, models.DO_NOTHING, db_column='no_ktp_pemesan')
    status = models.ForeignKey('Status', models.DO_NOTHING, db_column='status', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pemesanan'


class Pengembalian(models.Model):
    no_resi = models.ForeignKey('Pengiriman', models.DO_NOTHING, db_column='no_resi', primary_key=True)
    id_pemesanan = models.CharField(max_length=10, blank=True, null=True)
    metode = models.TextField()
    ongkos = models.FloatField()
    tanggal = models.DateField()
    no_ktp_anggota = models.ForeignKey(Alamat, models.DO_NOTHING, db_column='no_ktp_anggota', blank=True, null=True)
    nama_alamat_anggota = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pengembalian'


class Pengguna(models.Model):
    no_ktp = models.CharField(primary_key=True, max_length=20)
    nama_lengkap = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    tanggal_lahir = models.DateField(blank=True, null=True)
    no_telp = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pengguna'


class Pengiriman(models.Model):
    no_resi = models.CharField(primary_key=True, max_length=10)
    id_pemesanan = models.ForeignKey(Pemesanan, models.DO_NOTHING, db_column='id_pemesanan', blank=True, null=True)
    metode = models.TextField()
    ongkos = models.FloatField()
    tanggal = models.DateField()
    no_ktp_anggota = models.ForeignKey(Alamat, models.DO_NOTHING, db_column='no_ktp_anggota', blank=True, null=True)
    nama_alamat_anggota = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pengiriman'


class Status(models.Model):
    nama = models.CharField(primary_key=True, max_length=50)
    deskripsi = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'status'
