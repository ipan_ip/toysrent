from django.shortcuts import render

#Create a content paragraph for your landing page:

def index(request):
    response = {}
    return render(request, 'mainpage.html', response)