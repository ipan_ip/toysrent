from multiprocessing import connection

from django.shortcuts import render
from main.models import *

# Create your views here.
def index(request):
    response = {}
    return render(request, 'barang.html', response)

def ulasan(request):
    response = {}
    return render(request, 'ulasan_barang.html', response)

def list_item(request):
    response = {}
    response['daftar_item'] = Item.objects.raw("SELECT * FROM item")
    return render(request, 'barang.html', response)

def create_barang(request):
    response={}
    if request.method == "POST":
        response['id_barang'] = request.POST['id_barang']
        response['nama_item'] = request.POST['nama_item']
        response['warna'] = request.POST['warna']
        response['url_foto'] = request.POST['url_foto']
        response['kondisi'] = request.POST['kondisi']
        response['lama_penggunaan'] = request.POST['lama_penggunaan']
        response['personal_royalty_1'] = request.POST['personal_royalty_1']
        response['harga_sewa_1'] = request.POST['harga_sewa_1']
        response['personal_royalty_2'] = request.POST['personal_royalty_2']
        response['harga_sewa_2'] = request.POST['harga_sewa_2']
        response['personal_royalty_3'] = request.POST['personal_royalty_3']
        response['harga_sewa_3'] = request.POST['harga_sewa_3']
        with connection.cursor() as c:
            sql = "INSERT INTO Barang(id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan) VALUES ('%s','%s',%d,%d,'%s')" % (
                response['id_barang'], response['nama_item'], response['warna'], response['url_foto'], response['kondisi'], response['lama_penggunaan'])
            c.execute(sql)
        # response[''] = request.POST['']