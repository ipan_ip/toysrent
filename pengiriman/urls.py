from django.urls import path
from . import views

app_name = 'pengiriman'
urlpatterns = [
    path('daftar_pengiriman', views.kirim_list, name='kirim_list')
]