from django.urls import path

from . import views

app_name = 'barang'
urlpatterns = [
    path('', views.index, name='index'),
    path('ulasan_barang',views.ulasan,name='ulasan')
]